package core.annotations;

/**
 * Created by Ismail on 4/3/2018.
 * This Class used to retrieve annotations values related to test level
 * or method level on current runtime as defined in current test case
 * and globalize these values for any class call defined methods here
 * This class won't check if attribute has a value or not, so you need
 * to be sure value of the attribute is well defined and provided
 */
public class AttributesConfig {

    // This method to retrieve current value of data file
    public String getDataFile(ClassAttributes classAttributes) {
        // Verify if data File attribute not null to return it's value else return an empty string
        return classAttributes.DataFile() != null ? classAttributes.DataFile() : "";
    }

    // Retrieves default test file for the class
    public String getTestFile(ClassAttributes classAttributes) {
        return classAttributes.TestFile();
    }

    // This method to retrieve current value of BaseURL
    public String getBaseURL(ClassAttributes classAttributes) {
        // Verify if BaseURL attribute not null to return it's value else return an empty string
        return classAttributes.BaseURL() != null ? classAttributes.BaseURL() : "";
    }

    // This method to retrieve current value of CapabilitiesFile
    public String getCapabilitiesFile(ClassAttributes classAttributes) {
        // Verify if CapabilitiesFile attribute not null else will returns empty string
        return classAttributes.CapabilitiesFile() != null ? classAttributes.CapabilitiesFile() : "";
    }

    // This method to retrieve current value of data row
    public String getTestFile(MethodAttributes methodAttributes) {
        return methodAttributes.TestFile();
    }

    // This method to retrieve current value of data row
    public int getDataRow(MethodAttributes methodAttributes) {
        return (methodAttributes.DataRow());
    }

    // This method to retrieve current value of data columns
    public int[] getDataColumns(MethodAttributes methodAttributes) {
        return methodAttributes.DataColumns();
    }
}
