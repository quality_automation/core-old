package core.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;

/**
 * Created by Ismail on 3/23/2018.
 * This Annotation responsible for saving and initialize some attributes
 * related to test cases inside a specific class level only
 */
// This annotation to give RUNTIME policy only, This means these annotations will be executed on runtime level
@Retention(RetentionPolicy.RUNTIME)
//This annotation to give annotation scope on class level
@Target({TYPE})
public @interface ClassAttributes {

    // This attribute to parse test cases data that required to execute a group of test cases or one test case
    String DataFile() default "";

    // This attribute to parse Base URL of Project for current test method to run test case on required server\
    String BaseURL() default "";

    // This attribute saves test case format file
    String TestFile() default "";

    // This attribute to parse capabilities file for mobile test method
    String CapabilitiesFile() default "";
}
