package core.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;

/**
 * Created by Ismail on 4/3/2018.
 * This Annotation responsible for saving and initialize some attributes
 * related to test methods inside a specific class for methods level only
 */
// This annotation to give RUNTIME policy only
// This means these annotations will be executed on runtime level
@Retention(RetentionPolicy.RUNTIME)
//This annotation to give annotation scope
// on method level
@Target({METHOD})
public @interface MethodAttributes {

    // This attribute to parse data row of data file for current test method
    int DataRow() default 0;

    // This attribute to parse specific data from a DataRow
    int[] DataColumns() default 0;

    // This attribute to parse test file for test method
    String TestFile() default "";
}
