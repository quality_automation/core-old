package core.internal;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

/**
 * Created by Ismail on 12/25/2017.
 * This Class contains all main Framework Paths and variables
 */
public class EnvironmentInterface {

    /*************** Class Finals Section ***************/
    // Define project source code path
    public static final String SRC_PATH = "src/";
    // Define main folder path
    public static final String MAIN_PATH;
    // Define test folder path
    public static final String TEST_PATH;
    // Define main java folder path
    public static final String JAVA_MAIN_PATH;
    // Define main resources folder path
    public static final String RESOURCES_MAIN_PATH;
    // Define test java folder path
    public static final String JAVA_TEST_PATH;
    // Define test resources folder path
    public static final String RESOURCES_TEST_PATH;
    // Define temp resources folder path
    public static final String TEMP_RESOURCES_PATH;
    // Define report folder path
    public static final String REPORT_PATH;
    // Define screenShot folder path
    public static final String SCREEN_SHOT_PATH;
    // Define explicit wait of time in milliseconds
    public static final int EXPLICIT_WAIT_TIMEOUT;
    // Define current time of suite execution
    public static final String CURRENT_TIME;
    // Define sleep timeout variable
    public static final int SLEEP_TIMEOUT;
    // Define attempts timeout variable
    private static final int ATTEMPT_TIMEOUT;
    // Define default time unit for framework
    public static final Duration TIME_DURATION;

    /*************** Class Methods Section ***************/
    // This Definition of all finals in class
    static {
        MAIN_PATH = SRC_PATH + "main/";
        TEST_PATH = SRC_PATH + "test/";
        JAVA_MAIN_PATH = MAIN_PATH + "java/";
        RESOURCES_MAIN_PATH = MAIN_PATH + "resources/";
        JAVA_TEST_PATH = TEST_PATH + "java/";
        RESOURCES_TEST_PATH = TEST_PATH + "resources/";
        TEMP_RESOURCES_PATH = RESOURCES_TEST_PATH + "temp/";
        REPORT_PATH = "target/extent-reports/";
        SCREEN_SHOT_PATH = "screenshots/";
        EXPLICIT_WAIT_TIMEOUT = 400;
        CURRENT_TIME = LocalDateTime.now().withNano(0).toString().replace(":", "-");
        SLEEP_TIMEOUT = 50;
        ATTEMPT_TIMEOUT = 150;
        TIME_DURATION = Duration.ofMillis(ATTEMPT_TIMEOUT);
    }
}
