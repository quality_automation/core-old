package core;

import core.annotations.AttributesHandler;
import core.annotations.AttributesConfig;
import core.annotations.ClassAttributes;
import core.annotations.MethodAttributes;
import core.reports.TestReporter;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.testng.ITestContext;
import org.testng.ITestResult;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;

import static core.handlers.StringHandler.isStringBlank;
import static core.annotations.AttributesHandler.getClassDataFile;
import static core.internal.EnvironmentInterface.TEMP_RESOURCES_PATH;

/**
 * Created by Ismail on 12/25/2017.
 * This class contains all configuration methods related to TestNG
 * that will be applied overall project
 */
public class TestNGMethods {

    /*************** Class Variables Section ***************/
    // This variable saves current running test class
    private static Class runningTestClass = null;
    // This variable saves current running test method
    private static Method runningTestMethod = null;
    // This variable define an object of attributes config class to get all annotations
    AttributesConfig attributesConfig = new AttributesConfig();

    /*************** Class Methods Section ***************/
    // This method retrieves runningTestClass value
    public static Class getRunningTestClass() {
        return runningTestClass;
    }

    // This method saves runningTestClass value
    public static void setRunningTestClass(Class runningTestClass) {
        TestNGMethods.runningTestClass = runningTestClass;
    }

    // This method retrieves runningTestMethod value
    public static Method getRunningTestMethod() {
        return runningTestMethod;
    }

    // This method saves runningTestMethod value
    public static void setRunningTestMethod(Method runningTestMethod) {
        TestNGMethods.runningTestMethod = runningTestMethod;
    }

    // This method run automatically before each TestNG Suite
    public void setUpSuite(ITestContext testContext) {
        // Create and Start report and log test run status
        TestReporter.prepareReport(testContext);
    }

    // This method run automatically after each TestNG Suite
    public void tearDownSuite() {
        // Save all report to report file and close the file
        TestReporter.generateFinalReport();
    }

    // This method run automatically before each TestNG Test
    public void setUpTest(ITestContext testContext) {
        // Prepare Test Report, Start logging Test case status in report
        TestReporter.prepareTestReport(testContext);
    }

    // This method run automatically after each TestNG Test
    // AlwaysRun means should be run(no exclude for any reason)
    public void tearDownTest() {
        /******************** Method Report Section Start ********************/
        // Generate Test Report and write to full report
        TestReporter.generateTestReport();
        /******************** Method Report Section End ********************/
        // Delete Temp Folder with all files
        try {
            FileUtils.deleteDirectory(new File(TEMP_RESOURCES_PATH));
        } catch (IOException e) {
        }
    }

    // This method run automatically before each TestNG Class
    public void setUpClass() {
        /******************** Test Custom Annotation Section Start ********************/
        // Retrieve Test attributes from @ClassAttributes annotation
        String dataFile = "", testFile = "";
        // Save current running test class object
        setRunningTestClass(this.getClass());
        // Check if Annotation class is defined then save to object
        ClassAttributes classAttributes = null;
        if (getRunningTestClass().getAnnotation(ClassAttributes.class) != null)
            classAttributes = this.getClass().getAnnotation(ClassAttributes.class);
        // Retrieve all related test attribute values
        if (classAttributes != null) {
            testFile = attributesConfig.getTestFile(classAttributes);
            dataFile = attributesConfig.getDataFile(classAttributes);
        }
        // Save default test file attribute
        if (!isStringBlank(testFile))
            AttributesHandler.setClassTestFile(testFile);
        // Save data file attribute
        if (!isStringBlank(dataFile))
            AttributesHandler.setClassDataFile(dataFile);
        /******************** Test Custom Annotation Section End ********************/
    }

    // This method run automatically after each TestNG Class
    public void tearDownClass() {
        // Reset all old class files by calling AttributesHandler.resetClassAttributes
        AttributesHandler.resetClassAttributes();
    }

    // This method run automatically before each TestNG Method
    public void setUpMethod(ITestContext testContext, Method method) {
        /******************** Method Custom Annotation Section Start ********************/
        // Retrieve Method attributes from @ClassAttributes annotation, Define all method attribute variables
        String testFile = "";
        int dataRow = -1, dataColumns[] = {};
        // Check if Annotation class is defined then save to object
        MethodAttributes methodAttributes = null;
        try {
            // Save current running test method object
            setRunningTestMethod(getRunningTestClass().getMethod(method.getName()));
            // Define test case attributes and save to use them getting annotations values
            if (getRunningTestMethod().getAnnotation(MethodAttributes.class) != null)
                methodAttributes = getRunningTestMethod().getAnnotation(MethodAttributes.class);
        } catch (NoSuchMethodException e) {
            TestReporter.error("Warning: Something went wrong while define method attributes", false);
        }
        // Retrieve all related test attribute values
        if (methodAttributes != null) {
            testFile = attributesConfig.getTestFile(methodAttributes);
            dataRow = attributesConfig.getDataRow(methodAttributes);
            dataColumns = attributesConfig.getDataColumns(methodAttributes);
        }
        // Check if Data File is TXT or CSV
        String fileType = FilenameUtils.getExtension(getClassDataFile());
        // Check if test file isn't empty
        if (!isStringBlank(testFile))
            AttributesHandler.setMethodTestFile(testFile);
        // Check if method data row isn't invalid integer
        if (dataRow > 0)
            AttributesHandler.setMethodDataRow(AttributesHandler.getDataRow(fileType, dataRow));
        // Check if method data columns isn't empty to assign, This under implementation
        if (dataColumns.length != 0)
            ;
        /******************** Method Custom Annotation Section End ********************/

        /******************** Method Report Section Start ********************/
        // Start logging Test case status in report
        TestReporter.prepareMethodReport(method);
        // Add Categories of Test Case to report
        TestReporter.addMethodCategories(testContext, method);
        /******************** Method Report Section End ********************/
    }

    // This method run automatically after each TestNG Method
    public void tearDownMethod(ITestResult testResult) {
        // Reset Method Variables values by calling AttributesHandler.resetMethodAttributes
        AttributesHandler.resetMethodAttributes();
        // Generate Test Case Report and write to full report
        TestReporter.generateMethodReport();
        // Reset Class and Method objects to null values
        setRunningTestMethod(null);
        setRunningTestClass(null);
    }
}
