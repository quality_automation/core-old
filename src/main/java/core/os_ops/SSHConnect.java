package core.os_ops;

import org.apache.sshd.client.SshClient;
import org.apache.sshd.client.session.ClientSession;
import org.testng.annotations.Test;

import java.io.IOException;

/**
 * Created by Ismail on 3/24/2018.
 * This class contains all methods related to SSH service
 * To connect servers over SSH
 */
public class SSHConnect {
@Test
    // This method to connect server over SSH
    // all data are provided from info file
    public static void getSSHConnection(){
        // Retrieve ssh info data from file
        String username = "ec2-user";//ReadPropFile.getProperty(sshInfoFile, "username");
        String hostname = "demo.winfooz.com";//ReadPropFile.getProperty(sshInfoFile, "hostname");
        // Define
        SshClient sshClient = SshClient.setUpDefaultClient();
//        sshClient.addPublicKeyIdentity("");
        sshClient.start();
        ClientSession clientSession = null;
        try {
            System.out.println(sshClient.connect(username, hostname, 22).isConnected());
            clientSession = sshClient.connect(username, hostname, 22).getSession();
            clientSession.getConnectAddress();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //return clientSession;
    }
}
