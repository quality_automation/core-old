package core.files.validator;

import core.reports.TestReporter;

import java.io.File;

/**
 * Created by Ismail on 12/26/2017.
 * This class contains all related methods to validate if file status
 * is correct and checking validity of the file
 */
public class FileStatusValidator {

    /*************** Class Methods Section ***************/
    // This method to validate if file exist then return true
    public static Boolean isFileExist(String filePath) {
        return new File(filePath).exists();
    }

    // This method validate if Provided File is file or directory
    public static Boolean verifyFileIsFolder(String filePath) {
        return new File(filePath).isDirectory();
    }

    // This method validate if Provided File contains Data or Empty
    public static Boolean verifyFileContainsData(String filePath) {
        return new File(filePath).length() != 0;
    }

    // This method validate File status overall, If File Path exist, not a directory and contains data
    public static void verifyFileStatus(String filePath) {
        if (!isFileExist(filePath)) {
            TestReporter.error("File isn't exist, Please check File Path: " + filePath, true);
        }

        if (verifyFileIsFolder(filePath)) {
            TestReporter.error("The Provided a folder not a file, Please check Folder Path: " + filePath, true);
        }

        if (!verifyFileContainsData(filePath)) {
            TestReporter.error("The Provided an Empty File, Please check file Path: " + filePath, true);
        }
    }
}
