package core.files.writer;

import core.reports.TestReporter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import static core.files.FileHelper.createFolders;

/**
 * Created by Ismail on 6/16/2018.
 * This class contains all related methods for writing TXT File
 */
public class WriteTXTFile {

    /*************** Class Methods Section ***************/
    // This method to write into TXT File
    public static void writeTxtFile(String filePath, String string) {
        // Create file parent directories
        createFolders(filePath);

        // Define BufferedWriter variable
        BufferedWriter writer;
        try {
            writer = new BufferedWriter(new FileWriter(filePath, true));
            writer.append(string);
            writer.append("\n");
            writer.close();
        } catch (Throwable throwable) {
            TestReporter.error("Failed to write a string >> " + string + " to TXT File " + filePath + "Please check error message: " + throwable.getMessage(), true);
        }
    }
}
