package core.files.writer;

import core.reports.TestReporter;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;

import static core.files.reader.ReadExcelFile.*;

/**
 * Created by Ismail on 10/6/2018.
 * This class contains all methods related to write to excel file
 */
public class WriteExcelFile {

    /*************** Class Variables Section ***************/
    private static String excelFileName = "", sheetName = "";
    private static Workbook workbook = null;
    private static Sheet sheet = null;
    private static Row row = null;

    /*************** Class Methods Section ***************/
    // This method retrieves an Excel workbook
    private static void getExcelWorkbook(String excelFile) {
        try {
            // Save excel File name
            excelFileName = excelFile;
            // Create directories path for excel file
            new File(excelFile).getParentFile().mkdirs();
            // Create new Excel File
            if (new File(excelFile).exists()) {
                // Retrieve excel workbook
                workbook = WorkbookFactory.create(new FileInputStream(new File(excelFileName)));
            } else {
                // Create a new excel workbook
                workbook = new XSSFWorkbook();
            }
        } catch (Throwable throwable) {
            TestReporter.error("Something went wrong", throwable, false);
        }
    }

    // This method write excel workbook to excel file
    private static void writeExcelWorkbook() {
        try {
            if (!excelFileName.isEmpty()) {
                // Save excel file
                FileOutputStream fileOut = new FileOutputStream(excelFileName);
                workbook.write(fileOut);
                fileOut.flush();
                fileOut.close();
                workbook.close();
            } else {
                TestReporter.error("Please provide excel file name to write.", true);
            }
        } catch (Throwable throwable) {
            TestReporter.error("Something went wrong while writing to excel file.", throwable, true);
        }
    }

    // This method retrieves an excel sheet from the workbook
    private static void getExcelSheet(String excelFile, String sheetName) {
        try {
            // Save excel sheet name
            WriteExcelFile.sheetName = sheetName;
            // Retrieve excel workbook
            getExcelWorkbook(excelFile);
            // Retrieve excel sheet
            if (workbook.getSheet(sheetName) != null)
                sheet = workbook.getSheet(sheetName);
            else // Create excel sheet
                sheet = workbook.createSheet(sheetName);
        } catch (Throwable throwable) {
            TestReporter.error("Something went wrong while read excel sheet " + sheetName, throwable, true);
        }
    }

    // This method retrieves excel sheet row
    private static void getSheetRow(String excelFile, String sheetName, int rowNo) {
        // Retrieve excel sheet
        getExcelSheet(excelFile, sheetName);
        // Adjust Row no for Index, If you change this line review getLastRowIndex method
        rowNo = rowNo - 1 > 0 ? rowNo - 1 : 0;
        // Save row from excel sheet
        if (workbook.getSheet(sheet.getSheetName()).getRow(rowNo) != null)
            row = workbook.getSheet(sheet.getSheetName()).getRow(rowNo);
        else
            row = workbook.getSheet(sheet.getSheetName()).createRow(rowNo);
    }

    // This method retrieves excel sheet row
    private static void getLastSheetRow(String excelFile, String sheetName) {
        // Retrieve excel sheet
        getExcelSheet(excelFile, sheetName);
        // Retrieve last row index with getLastRowIndex method then Save row from excel sheet
        getSheetRow(excelFile, sheetName, getLastRowIndex());
    }

    // This method create a new cell to exiting row
    private static Row addRowCell(Row row, int cellNo, String cellData) {
        // Check if Cell exists in the Row
        if (row.getCell(cellNo) != null)
            row.getCell(cellNo).setCellValue(cellData);
        else
            row.createCell(cellNo).setCellValue(cellData);
        return row;
    }

    // This method retrieve last row in a sheet
    private static int getLastRowIndex() {
        // I need to add 2 cause of: Method return last row has data, decrease 1 by getSheetRow method
        if (sheet != null)
            return sheet.getLastRowNum() + 2;
        else
            return 0;
    }

    // This method writes a row to excel file
    private static void writeDataToExcelRow(Row row, ArrayList<String> rowData) {
        // Write new Data to the row
        int counter = 0;
        for (String cellData : rowData) {
            addRowCell(row, counter, cellData);
            counter++;
        }
        // Write Updated Row to excel file
        writeExcelWorkbook();
    }

    // This method writes a row to excel sheet with array of Strings
    public static void writeRowToExcelSheet(String excelFile, String sheetName, int rowNo, ArrayList<String> rowData) {
        // Retrieve excel row from the sheet
        getSheetRow(excelFile, sheetName, rowNo);
        // Write new Data to the row
        writeDataToExcelRow(row, rowData);
    }

    // This method writes a row to excel sheet with array of Strings with sheetNo parameter
    public static void writeRowToExcelSheet(String excelFile, int sheetNo, int rowNo, ArrayList<String> rowData) {
        // Get Sheet Name then use writeRowToExcelSheet method to Write new Data to the row
        writeRowToExcelSheet(excelFile, getSheetName(excelFile, sheetNo), rowNo, rowData);
    }

    // This method writes a row in the last of excel sheet with array of Strings
    public static void writeRowToExcelSheet(String excelFile, String sheetName, ArrayList<String> rowData) {
        // Retrieve excel row from the sheet
        getLastSheetRow(excelFile, sheetName);
        // Write new Data to the row
        writeDataToExcelRow(row, rowData);
    }

    // This method writes a row in the last of excel sheet with array of Strings
    public static void writeRowToExcelSheet(String excelFile, int sheetNo, ArrayList<String> rowData) {
        // Retrieve sheet name and write row data to the sheet
        writeRowToExcelSheet(excelFile, getSheetName(excelFile, sheetNo), rowData);
    }

    // This method writes a row to excel sheet with ArrayList parameter
    public static void writeHeaderToExcelSheet(String excelFile, String sheetName, ArrayList<String> rowData) {
        // Retrieve excel row from the sheet
        getSheetRow(excelFile, sheetName, 0);
        // Style Row cells
        // Create a Font for styling header cells
        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 14);
        headerFont.setColor(IndexedColors.RED.getIndex());
        // Create a CellStyle with the font
        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        // Create a header Row
        int counter = 0;
        for (String cellData : rowData) {
            Cell cell = row.createCell(counter);
            cell.setCellValue(cellData);
            cell.setCellStyle(headerCellStyle);
            counter++;
        }
        // Write new Data to header row
        writeDataToExcelRow(row, rowData);
    }

    // This method writes a row to excel sheet with Array parameter
    public static void writeHeaderToExcelSheet(String excelFile, String sheetName, String[] rowData) {
        // Retrieve excel row from the sheet
        getSheetRow(excelFile, sheetName, 0);
        // Style Row cells
        // Create a Font for styling header cells
        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 14);
        headerFont.setColor(IndexedColors.RED.getIndex());
        // Create a CellStyle with the font
        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        // Create a header Row
        int counter = 0;
        for (String cellData : rowData) {
            Cell cell = row.createCell(counter);
            cell.setCellValue(cellData);
            cell.setCellStyle(headerCellStyle);
            counter++;
        }
        // Convert Array to ArrayList
        ArrayList<String> rowDataList = new ArrayList<>(Arrays.asList(rowData));
        // Write new Data to header row
        writeDataToExcelRow(row, rowDataList);
    }

    // This method writes Data to existing excel row(complete existing data with new ones)
    public static void writeToExcelRow(String excelFile, String sheetName, int rowNo, ArrayList<String> rowData) {
        // Read current data from excel file
        if (isRowExist(excelFile, sheetName, rowNo)) {
            ArrayList<String> currentData = getRowData(excelFile, sheetName, rowNo);
            // Save current Data with new Data
            currentData.addAll(rowData);
            writeRowToExcelSheet(excelFile, sheetName, rowNo, currentData);
        } else {
            writeRowToExcelSheet(excelFile, sheetName, rowNo, rowData);
        }
    }

    // This method writes Data to existing excel row(complete existing data with new ones)
    public static void writeToExcelRow(String excelFile, int sheetNo, int rowNo, ArrayList<String> rowData) {
        // Get Sheet Name then use writeToExcelRow method to write row data
        writeToExcelRow(excelFile, getSheetName(excelFile, sheetNo), rowNo, rowData);
    }

    // This method writes Data to existing excel header with sheet name parameter
    public static void writeToExcelHeader(String excelFile, String sheetName, ArrayList<String> rowData) {
        // Write data to header by writeToExcelRow method
        writeToExcelRow(excelFile, sheetName, 1, rowData);
    }

    // This method writes Data to existing excel header with sheet number parameter
    public static void writeToExcelHeader(String excelFile, int sheetNo, ArrayList<String> rowData) {
        // Write data to header by writeToExcelRow method
        writeToExcelRow(excelFile, getSheetName(excelFile, sheetNo), 1, rowData);
    }

    // This method merges two excel sheets into new one
    public static void mergeTwoExcelSheets(String firstExcel, String secondExcel, int firstSheet, int secondSheet, String outputExcel, int outputSheet) {
        // Read excel sheets Data
        ArrayList<ArrayList<String>> firstDataSheet = getSheetData(firstExcel, firstSheet);
        ArrayList<ArrayList<String>> secondDataSheet = getSheetData(secondExcel, secondSheet);

        // Merge two sheets into one
        int dataSize = firstDataSheet.size() > secondDataSheet.size() ? firstDataSheet.size() : secondDataSheet.size();
        for (int counter = 0; counter < dataSize; counter++) {
            // Get same row data to merge them
            ArrayList<String> firstRow = firstDataSheet.size() < counter ? firstDataSheet.get(counter) : null;
            ArrayList<String> secondRow = secondDataSheet.size() < counter ? secondDataSheet.get(counter) : null;
            // Exclude empty cells of rows and merge them into one
            ArrayList<String> newRow = new ArrayList<>();
            if (firstRow != null)
                newRow.addAll(firstRow);
            if (secondRow != null)
                newRow.addAll(secondRow);
            // Write merged row into new excel file and sheet
            if (newRow.size() > 0)
                writeRowToExcelSheet(outputExcel, outputSheet, newRow);
        }
    }
}
