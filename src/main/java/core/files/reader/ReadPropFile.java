package core.files.reader;

import core.reports.TestReporter;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.Set;

import static core.files.FileHelper.getFileAbsolutePath;
import static core.files.validator.FileExtensionValidator.verifyPropertiesFileType;
import static core.files.validator.FileStatusValidator.isFileExist;
import static core.files.validator.FileStatusValidator.verifyFileStatus;

/**
 * Created by Ismail on 12/26/2017.
 * This class contains all related methods for Read From
 * Properties File (*.properties) and do actions or retrieve data
 * from the file
 */
public class ReadPropFile {

    // Note: we need to implement method of read prop file without checking file type

    /*************** Class Methods Section ***************/
    // This method read File Properties content and return all contents as Properties Object
    private static Properties readPropFile(String propFile) {
        // Check if provided file name only without path then add test resources full path
        if (!isFileExist(propFile))
            // build full path for properties file
            propFile = getFileAbsolutePath(propFile);
        // Verify propFile Status
        verifyFileStatus(propFile);
        // Verify propFile content Type
        // verifyPropertiesFileType(propFile);
        // Initialize Properties Object
        Properties properties = new Properties();
        // Start operating to Load Properties from file
        try {
            // Read The Provided File content as inputStream
            InputStream inputStream = new FileInputStream(propFile);
            // Load inputStream Object to Properties
            properties.load(inputStream);
            // Convert all Properties Keys to Lower Case
            Set<Object> keys = properties.keySet();
            // Define a new Properties object to save all keys,values there
            Properties newProperties = new Properties();
            for (Object key : keys) {
                // Get all Properties Keys
                String propKey = (String) key;
                // Get Prop key and Prop Value
                String propValue = properties.getProperty(propKey);
                //properties.remove(propKey);// Remove the key,value that saved
                // Save the key, value again but after convert Key toLowerCase
                newProperties.setProperty(propKey.toLowerCase(), propValue);
            }
            // Assign newProperties that contains lower case keys To properties object
            properties = newProperties;
            // Return Properties Object
            return properties;
        } catch (Throwable throwable) {
            // In case something went wrong while reading Properties file then add error to report and fail test case
            TestReporter.error("Something went wrong in reading Properties file: " + propFile, throwable, true);
        }
        // Default return is null
        return null;
    }

    // This method return a value string of provided Key from Properties File
    public static String getProperty(String propFile, String propertyName) {
        // Check if Property Name exist and if yes then return value as string
        if (readPropFile(propFile).containsKey(propertyName.toLowerCase()))
            return readPropFile(propFile).getProperty(propertyName.toLowerCase());
        else
            // Else will return an empty string
            return "";
    }
}
