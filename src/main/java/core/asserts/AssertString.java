package core.asserts;

import org.testng.Assert;
import core.reports.TestReporter;

/**
 * Created by Ismail on 12/25/2017.
 * This class contains Assertions related to Strings only
 */
public class AssertString {

    /*************** Class Methods Section ***************/
    // This method compare two strings if equals and fails if not equal
    // If this method fail then test case will fail
    public static void assertEqualStrings(String actual, String expected, String errMessage) {
        try {// Assert if two strings are equal then pass
            Assert.assertEquals(actual, expected, errMessage);
        } catch (Throwable throwable) {
            // If assert fails then will throw an error and catch
            // that error to report it as step fails
            TestReporter.fail(throwable.getMessage(), true);
        }
    }

    // This method check if string is empty/null if yes then fail the test case
    public static void assertNotEmptyString(String actual, String errMessage) {
        // verify if string is empty then fail the test case else do nothing
        try {
            Assert.assertNotNull(actual, errMessage);
            Assert.assertFalse(actual.isEmpty(), errMessage);
        } catch (Throwable throwable) {
            TestReporter.fail(throwable.getMessage(), true);
        }
    }
}
