package core.handlers.scripting;

import core.reports.TestReporter;

import static core.files.FileHelper.createTempFile;
import static core.files.FileHelper.getFileAbsolutePath;
import static core.files.validator.FileStatusValidator.isFileExist;
import static core.os_ops.Services.getProcessOutput;

/**
 * Created by Ismail on 10/6/2018.
 */
public class JSHandler {

    // This method executes javascript commands throw command line
    private static String jsExecutor(String jsFile) {
        // Check if NPM, Node are installed to continue executing script
        if (!isNodeInstalled() && !isNPMInstalled())
            TestReporter.error("NodeJS or NPM isn't installed on your computer, for info about installation visit this site: https://nodejs.org/en/download/", true);

        // Call Process executor to run command
        return getProcessOutput("node " + jsFile);
    }

    // This method to execute js commands with input String parameter
    public static String runJScriptCommand(String jsInput) {
        // Save input data to JS file
        String jsFile = createTempFile("run.js", jsInput);
        return jsExecutor(jsFile);
    }

    // This method to execute js commands with File parameter
    public static String runJScriptFile(String jsFileName) {
        // Check if file exist and get file absolute path name
        if (!isFileExist(jsFileName))
            // build full path for txtFile
            jsFileName = getFileAbsolutePath(jsFileName);
        return jsExecutor(jsFileName);
    }

    // This method verifies nodeJS is installed
    private static boolean isNodeInstalled() {
        return !getProcessOutput("node -v").contains("not recognized");
    }

    // This method verifies NPM is installed
    private static boolean isNPMInstalled() {
        return !getProcessOutput("npm -v").contains("not recognized");
    }

    // This method verifies nodeJS library installed or not
    public static boolean isNodeLibraryInstalled(String libraryName) {
        return getProcessOutput("npm ls --depth=0").contains(libraryName);
    }

    // This method installs nodeJS libraries
    public static void setNodeLibraries(String... libraries) {
        for (String library : libraries) {
            getProcessOutput("npm i " + library);
        }
    }
}
