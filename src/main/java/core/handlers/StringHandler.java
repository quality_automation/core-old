package core.handlers;

import org.apache.commons.validator.routines.UrlValidator;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.stream.Collectors;

/**
 * Created by Ismail on 12/25/2017.
 * This class contains all related methods for processing strings
 * and do some operations to The provided strings
 */
public class StringHandler {

    /*************** Class Methods Section ***************/
    // This method split String of provided expression and return asList
    public static String[] getStringAsArray(String string, String expressions) {
        return string.split(expressions) != null ? string.split(expressions) : null;
    }

    // This method validate Provided String as URL and return true or false
    public static boolean validateURL(String url) {
        // Validate if URL contains multiple //
        // and replace all duplicated slashes with one slash
        while (url.contains("//"))
            url = url.replace("//", "/");
        // Fix http:// issue cause in previous while it is removed
        // to be as one slash so we need to return that double slashes
        url = url.replaceFirst(":/", "://");
        // Check if URL contains localhost then will return true
        if (!url.contains("localhost"))// Else will validate the URL
            return UrlValidator.getInstance().isValid(url);
        else if (url.contains("localhost"))// If contains localhost return true
            return true;
        else// If URL is invalid and doesn't contains localhost then return false
            return false;
    }

    // This method clean the String from any return character
    public static String cleanStringFromReturn(String string) {
        return string.replaceAll("[\\n \\t ]", "");
    }

    // This method to generate a random string
    public static String getRandomString(int noOfChars) {
        // Define Upper case characters
        final String upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        // Define Lower case characters
        final String lower = upper.toLowerCase();
        // Define Digits numbers
        final String digits = "0123456789";
        // Define all alpha-numeric string
        final String alphaNum = upper + lower + digits;
        // Define StringBuilder to save generated String inside
        StringBuilder stringBuilder = new StringBuilder(noOfChars);
        // Define for loop to generate random string from alphaNum constant
        for (int counter = 0; counter < noOfChars; counter++) {
            // Generate random number within alphaNum length range then get that character from alphaNum constant (new SecureRandom().nextInt) and save it inside StringBuilder with append method
            stringBuilder.append(alphaNum.charAt(new SecureRandom().nextInt(alphaNum.length())));
        }
        // Return StringBuilder as a String
        return stringBuilder.toString();
    }

    // This method to get integer from given String
    public static int getIntegerFromString(String string) {
        return Integer.parseInt(string.replaceAll("[\\D]", ""));
    }

    // This method to converts String to HashMap
    public static HashMap<String, String> getHashMapFromString(String expression, String string) {
        string = string.substring(1, string.length() - 1);
        String[] keyValuePairs = string.split(",");
        HashMap<String, String> hashMap = new HashMap<>();

        for (String pair : keyValuePairs) {
            // Check Array index to prevent ArrayIndexOutOfBoundsException
            if (pair.split(expression).length != 2)
                continue;
            String key = pair.split(expression)[0];
            String value = pair.split(expression)[1];
            hashMap.put(key, value);
        }
        return hashMap;
    }

    // This method converts UpperCase Letter to _LowerCase String
    public static String convertUpperCaseToUnderScore(String string) {
        String convertedString = "";
        for (char character : string.toCharArray()) {
            String newChar = Character.isUpperCase(character) ? "_" + Character.toLowerCase(character) : String.valueOf(character);
            convertedString += newChar;
        }
        // Check if string starts with underscore to remove it(This for class name)
        return convertedString.startsWith("_") ? convertedString.replaceFirst("_", "") : convertedString;
    }

    // This method checks if String isn't null, empty or blank
    public static boolean isStringBlank(String string) {
        return string == null || string.trim().isEmpty();
    }

    // This method removes Pipelines from ArrayList elements
    public static ArrayList<String> replaceFromListElements(ArrayList<?> arrayList, String delimiter) {
        return arrayList.stream().map(element -> String.valueOf(element).replace(delimiter, ""))
                .collect(Collectors.toCollection(ArrayList::new));
    }
}
